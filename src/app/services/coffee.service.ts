import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, delay, filter, map, take, tap } from 'rxjs/operators';
import { forkJoin, from, Observable, of, throwError } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'PERSON-TYPE': 'GOOD PERSON',

  })
};

// 用于 类型约束， 如果不用 interface 的话
export class MyPost {

}


@Injectable({
  providedIn: 'root'
})
export class CoffeeService {

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<Coffee[]>('assets/list.json').pipe(
      delay(1000)
    );
  }

  getList() {
    return this.getAll().toPromise();
  }



  /***********************************************************************
   *******  Emit emitter at service
   ************************************************************************/
  public eventEmit: EventEmitter<number> = new EventEmitter();



  /***********************************************************************
   *******   以前的 RXJS 一些操作方式
   ************************************************************************/

  //　 必须发送header  'Content-Type': 'application/json',

  public postDotNetPersonList(data: any) {
    const url = 'https://localhost:5588/luyou9/json5';
    // return this.http.get(url, httpOptions);
    return this.http.post<any>(url, data, httpOptions);

  }


  public postPersonVerify(data: any) {
    const url = 'https://localhost:5588/luyou9/json6';
    // return this.http.get(url, httpOptions);
    return this.http.post<any>(url, data, httpOptions);

  }


  public getJsonData1() {
    let url = 'https://jsonplaceholder.typicode.com/comments?postId=1';
    return this.http.get(url);

  }


  public getJsonData2() {
    let url = 'https://jsonplaceholder.typicode.com/comments?postId=1';
    return this.http.get(url);
  }

  /////////////////////// FORK JOIN ////////////////////////////////////
  public forkJoinData() {
    let url1 = 'https://jsonplaceholder.typicode.com/comments?postId=1';
    let url2 = 'https://jsonplaceholder.typicode.com/comments?postId=1';
    return forkJoin([this.http.get(url1), this.http.get(url2)]);
  }


  //////////////////////// TO PROMISE  ////////////////////////////////////
  public async getJsonDataToPromise() {
    let url = 'https://jsonplaceholder.typicode.com/comments?postId=1';
    return this.http.get(url).toPromise();

  }


  //////////////////////// ASYNC STYLE  ////////////////////////////////////

  private userURL = 'https://jsonplaceholder.typicode.com/users/3';
  private newUrl = 'https://jsonplaceholder.typicode.com/comments?postId=1';
  product$ = this.http.get<any>(this.newUrl)
    .pipe(
      map(res => res)
    );

  // retry(3),
  //  take(2),
  //  tap(console.log('the tap')),
  // delay(2000),
  //catchError(console.warn('error here'))


  public getArticlePipe(): Observable<any> {
    return this.http.get<any>(this.userURL)
      .pipe(
        delay(2000),
        map(res => res.address),
        catchError(err => {
          console.warn('errer....');
          return of(null);
        })
      );
  }


  // 在这里重新组织好对象
  public getArticlePipe2(): Observable<any> {
    return this.http.get<any>(this.newUrl)
      .pipe(
        //******** 重新组织对象属性 ********
        map((data: any[]) =>
          data.map(
            item => {
              return {
                title: item.name,
                emailid: item.id,
                age: 123,
                ...item
              };
            })
        )
      );

  }


  public basePipe(): Observable<any> {
    return this.http.get<any>(this.newUrl)
      .pipe(
        map((data: any) => {
          return data;
        }), catchError(error => {
          return throwError('Something went wrong!');
        })
      );
    //   .catchError( return throwError( 'Something went wrong!' );)

  }


}
