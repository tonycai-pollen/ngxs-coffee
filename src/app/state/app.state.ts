import { State, Action, StateContext, Selector } from '@ngxs/store';

import { mergeMap, catchError, take, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  GetCoffeeListSuccess, GetCoffeeListFailed,
  AddToCart, RemoveCartItem,
  RemoveOneCartItem, EmptyCart,
  AddToCoffeeList,
  GetCoffeeList,
  DummySetState,
  AddOneCartItem
} from './app.actions';

import { CoffeeService } from '../services/coffee.service';

export const getAppInitialState = (): App => ({
  coffeeList: [],
  cart: []
});

// 初始化状态数据
@State<App>({
  name: 'app',
  defaults: getAppInitialState()
})

export class AppState {
  constructor(private coffeeSvc: CoffeeService) {
  }

  @Selector()
  public static coffeeList(state: App) {
    return state.coffeeList;
  }

  @Selector()
  public static totalCartAmount(state: App) {
    const priceList = state.cart
      .map(c => {
        const unitPrice = state.coffeeList.find(x => x.name === c.name).price;
        return unitPrice * c.quantity;
      });
    const sum = priceList.reduce((acc, curr) => acc + curr, 0);

    return sum;
  }

  @Selector()
  static totalCartQuantity(state: App) {
    const total = state.cart
      .reduce((acc, curr) => acc + curr.quantity, 0);

    return total;
  }

  @Action(GetCoffeeList)
  async getCoffeeList(ctx: StateContext<App>, action: GetCoffeeList) {
    try {
      const coffeeList = await this.coffeeSvc.getList();

      const state = ctx.getState();


      // 初始化 状态内容数据
      ctx.setState({
        ...state,
        coffeeList
      });
    } catch (error) {
      ctx.dispatch(new GetCoffeeListFailed(error));
    }

    console.log('@Action GetCoffeeList-->', ctx.getState());

  }

  // @Action(GetCoffeeList)
  // getCoffeeList$(ctx: StateContext<App>, action: GetCoffeeList) {
  //     return this.coffeeSvc.getAll().pipe(
  //         tap(coffeeList => {
  //             const state = ctx.getState();

  //             ctx.setState({
  //                 ...state,
  //                 coffeeList
  //             });
  //         }),
  //         catchError(() => ctx.dispatch(new GetCoffeeListFailed()))
  //     )

  // }

  // @Action(GetCoffeeList)
  // getCoffeeList$(ctx: StateContext<App>, action: GetCoffeeList) {
  //     return this.coffeeSvc.getAll()
  //         .pipe(
  //             mergeMap(x => ctx.dispatch(new GetCoffeeListSuccess(x))),
  //             catchError(() => ctx.dispatch(new GetCoffeeListFailed()))
  //         );
  // }

  // @Action(GetCoffeeListSuccess)
  // getCoffeeListSuccess(ctx: StateContext<App>, action: GetCoffeeListSuccess) {
  //     const state = ctx.getState();

  //     const current = {
  //         coffeeList: action.payload
  //     };

  //     ctx.setState({
  //         ...state,
  //         ...current
  //     });
  // }

  // @Action(GetCoffeeListFailed)
  // getCoffeeListFailed(ctx: StateContext<App>, action: GetCoffeeListFailed) {
  //     const state = ctx.getState();
  //     console.log('here');

  //     const current = {
  //         coffeeList: []
  //     };

  //     ctx.setState({
  //         ...state,
  //         ...current
  //     });
  // }

  @Action([AddToCart, AddOneCartItem])  // 对应 actions 类名
  // addToCart 是该作用域的函数名，可以换掉不影响 actions 操作, 底下action参数类型AddToCart可以不指定
   addToCart(ctx: StateContext<App>, action: AddToCart) {
    const state = ctx.getState();

    // find cart item by item name
    const { quantity = 0 } = state.cart.find(x => x.name === action.payload) || {};

    const current = {
      cart: [
        ...state.cart.filter(x => x.name !== action.payload),
        {
          name: action.payload, quantity: quantity + 1
        }
      ]
    };

  //  console.warn("aa",  {...state, ...current}); //cart 会代替，两者顺序不能反

    ctx.setState({
      ...state,
      ...current,
    //  like: [1,2,3]
    });

    // 可以随意加入数据属性 比如： like 属性
    console.log('current', current);
    console.log('@Action AddToCart-->', ctx.getState());
  }

  @Action(RemoveCartItem)
  removeCartItem(ctx: StateContext<App>, action) {
    const state = ctx.getState();

    const current = {
      cart: [...state.cart.filter(x => x.name !== action.payload)]
    };

    ctx.setState({
      ...state,
      ...current
    });

    console.log('@Action RemoveCartItem-->', ctx.getState());
  }

  @Action(RemoveOneCartItem)
  removeOneCartItem(ctx: StateContext<App>, action: RemoveOneCartItem) {
    const state = ctx.getState();

    const item = state.cart.find(x => x.name === action.payload);

    const current = {
      cart: [
        ...state.cart.filter(x => x.name !== action.payload),
        ...(item.quantity - 1 <= 0 ? [] : [{ name: item.name, quantity: item.quantity - 1 }])
      ]
    };

    ctx.setState({
      ...state,
      ...current
    });

    console.log('@Action RemoveOneCartItem-->', ctx.getState());
  }

  @Action(EmptyCart)
  emptyCart(ctx: StateContext<App>, action: EmptyCart) {
    const state = ctx.getState();

    const current = {
      cart: []
    };

    ctx.setState({
      ...state,
      ...current
    });

    console.log('@Action EmptyCart-->', ctx.getState());

  }

  @Action(AddToCoffeeList)
  addToCoffeeList(ctx: StateContext<App>, action: AddToCoffeeList) {
    const state = ctx.getState();

    const current = {
      coffeeList: [...state.coffeeList, ...action.payload]
    };

    ctx.setState({
      ...state,
      ...current
    });

    console.log('@Action AddToCoffeeList-->', ctx.getState());
  }

  @Action(DummySetState)
  dummySetState(ctx: StateContext<App>, action: DummySetState) {
    const state = ctx.getState();

    const current = {
      ...action.payload
    };

    ctx.setState({
      ...state,
      ...current
    });
  }
}
