import { Component, OnDestroy, OnInit } from '@angular/core';
import { CoffeeService } from '../../services/coffee.service';
import { from, Observable, of, Subscription } from 'rxjs';
import { catchError, filter, map, subscribeOn } from 'rxjs/operators';

@Component({
  selector: 'app-async-test',
  templateUrl: './async-test.component.html',
  styleUrls: ['./async-test.component.css']
})


export class AsyncTestComponent implements OnInit, OnDestroy {

  public asyncObservale: Observable<any>;   //可以直接在模版上 ASYNC

  private subscription: Subscription;     //用于取消订阅

  public product$ = this.coffeeSvc.product$; //Observable<any> 类型，可以直接在模版上 ASYNC，不需要再订阅
  // .pipe(
  //   catchError(err => {
  //     console.error('error here', err);
  //     return of(null);
  //   })
  // );

  public myData: any[];

  constructor(private coffeeSvc: CoffeeService) {
  }


  emitToService() {
    let theNumber: number = Math.floor(Math.random() * 10);
    this.coffeeSvc.eventEmit.emit(theNumber);
  }

  ngOnInit() {

    //from service event emitter
    this.coffeeSvc.eventEmit.subscribe(
      emitValue => {
        console.log('the emit number--', emitValue);
      },
      err => {
        console.warn(err);
      }
    );


    // Subscription 类型，用于取消订阅
    this.subscription = this.coffeeSvc.getArticlePipe().subscribe(value => {
        console.log('user info 只取一个字段 --', value);
      },
      error1 => {
        console.log(error1);
      });


    //Observable<any> 类型，可以直接在模版上 asyncObservale | ASYNC
    // 其实和product$ 的写法是一样的
    this.asyncObservale = this.coffeeSvc.getArticlePipe2();


    //基本用法
    this.coffeeSvc.basePipe().subscribe(
      val => {

        //**** 重新组织对象属性
        this.myData = val.map(
          item => {
            return {
              title: item.name,
              emailid: item.id,
              age: 88
            };
          });


        console.log('  array 基本用法-重新组织对象属性', this.myData);
        console.log('  array 基本用法val', val);
      },
      err => {
        console.log(err);
      }
    );

  }

  ngOnDestroy() {
    // 必须取消订阅
    this.subscription.unsubscribe();
  }
}



